# Investigating the Influence of Different Counting Conditions in Count Matrix based Code Clone Detection
#### by Lasse Schuirmann
##### Institute for Software Systems
### June 4, 2015

***

# Contents

- The Problem
  - Why do we Care?
  - What is a Code Clone?
  - Why is it Difficult to Detect Them?
- The Solution
  - A Similarity Definition
  - Grouping Variables
  - Comparing Functions
  - False Positive Avoidance
- Summary
- Investigation and Evaluation
  - Creating a Test Dataset
  - Visualize Results
  - Improvement Possibilities
  - Reproducability

***

# The Problem

***

## Why do we Care?

- **Smells**
- **Duplicate bugs**
- **Less Code**.

***

## Why do we Care?

#### What do we Want?

- **Opportunities** to **improve**
- Most **useful results**

***

## What is a Code Clone?

> A **code fragment** (...) is a **clone** of **another code fragment** if they
> are **similar by some given definition of similarity** (...).

*- Roy et al., Comparison and evaluation of code clone detection techniques and
tools: A qualitative approach*

***

## What is a Code Clone?

#### An Example

```C
bool test_search_for_empty() {
    int result;
    // Test without regexes active
    result = search_for("", "not_in_there", false)
    return result == -1;
}
```

```C
bool test_search_for_regex_empty() {
    int result;
    // Test with regexes active
    result = search_for("", "not_in_there", true)
    return result == -1;
}
```

***

## Why is it Difficult to Detect Them?

- **Similarity** is **hard to define**
- **A lot possible combinations**
- **Understanding code**

***

# The Solution

#### For C

***

# The Solution

#### Disclaimer

***

## A Similarity Definition

- **Represent variables** by **vectors**
- **Count different usage kinds**
- **Hashing**

***

## A Similarity Definition

#### Counting Conditions

**Count variables under specified circumstances**.

Example:

`(DECLARED, ASSIGNED, RETURNED)`

***

## A Similarity Definition

#### Example of a Count Vector

```C
int result; // DECLARED
// Test with regexes active
result = search_for("", "not_in_there", true) // ASSIGNED
return result == -1; // RETURNED
```

`(1, 1, 1)`

***

## A Similarity Definition

#### Comparing Count Vectors

```python
class CountVector:
    def difference(self, other):
        maxabs = sum(max(x, y)**2 for x, y in zip(self, other))
        if maxabs == 0: return 0
        return sum((x-y)**2 for x, y in zip(self, other))/maxabs
```

- **Zeros don't matter**
- **Normalized**
- **Symmetric**

***

## Grouping Variables

- Search for **duplicate functions**
- **Represent function** by it's **variables**
- Ignore **globals and members**
  - **Limitation**!

***

## Comparing Functions

- **Cost matrix**
- **Best match** of **count vectors**
- Matching complexity: `O(n^3)`

***

## Comparing Functions

### Creating the Cost Matrix

```python
cost_matrix = [[cv1.difference(cv2)
                for cv2 in cm2.values()]
               for cv1 in cm1.values()]
```

- **Unmatched**: Difference of one

***

# False Positive Avoidance

- **Common** code clones: **getters & setters**
  - Not useful
- **Heuristics**:
  - **Exclude small** functions

***

# Summary

- Create a **count vector** for each **variable**.
- **Compare functions** with **graph matching**.
- **Compare the result** with a **threshold**.

***

# Investigation and Evaluation

***

## Creating a Test Dataset

- Collect **clones** and **non-clones**
- **Unittests!**

***

## Creating a Test Dataset

#### How to Obtain the Test Dataset

- **Clone** samples:
  - **Test clone dataset by Roy et al.**
  - Own samples
- **Non-clone** samples:
  - **Real life code**

**Running** on **real code**!

***

## Creating a Test Dataset - Non-Clone Example

```C
void *memcpy(void *restrict dst, const void *restrict src, int len) {
    char *dt = (char*)dst;
    char *sc = (char*)src;
    while(len--) *dt++ = *sc++;
    return dst;
}
```

```C
void *memset(void * dst, int val, int len) {
    char *dt = (char*)dst;
    while(len--) *dt++ = (char)val;
    return dst;
}
```

***

## Creating a Test Dataset - Clone Example

```C
static void scrollUp() {
    uint16_t i;
    volatile unsigned char *videoram = (unsigned char *)FB_MEM_LOCATION;
    for(i=0; i<(FB_LINES-1)*FB_COLUMNS*2; i++)
        videoram[i]=videoram[i+(FB_COLUMNS * 2)];
    for(; i<FB_LINES*FB_COLUMNS*2; i++)
        if(i % 2) videoram[i]=COLCODE(STDFG, STDBG);
        else videoram[i]=BLANK; }
```

```C
void clearScreen() {
    uint16_t i;
    volatile unsigned char *videoram = (unsigned char *)FB_MEM_LOCATION;
    for(i=0; i<FB_LINES*FB_COLUMNS*2; i++)
        if(i % 2) videoram[i]=COLCODE(STDFG, STDBG);
        else videoram[i]=BLANK;
    setCursor(GET_POS(0,0)); }
```

***

## Visualize Results

![Result Visualization](https://storage.googleapis.com/b.swipeusercontent.com/rSefw9nz_hN_F67_ew1kIPNDwQCgsw-672.png)

- **Red**: *Clones*
- **Green**: *Non-clones*
- **x-Axis**: *Difference value*

**Goal**: **Maximize area between red and green**.

***

## Improvement Possibilities

- **Linux kernel yields false positives**
  - **Should count vector comparision** be **normalized**?
  - Count **function calls**!
- **Globals** and **members** are ignored.
- **Performance**.
  - **Fast bipartite graph matching algorithms** with **error estimation**
- **Evaluation**:
  - **Alternative tool comparision**

***

## Improvement Possibilities

#### Other Languages

- **No modification** to the **algorithm**
- **Rewrite counting conditions**

***

## Improvement Possibilities

#### Reusing Count Vectors

- **Identify other problems**

***

## Reproducability

```
git clone https://github.com/coala-analyzer/coala.git coala
cd coala
./run_tests.py -m
```

```
cd coala
sudo ./setup.py install  # Install coala
cd ../your_c_project
coala -s --bears=ClangCloneDetectionBear --files=**/*.c  # -s saves config
```

Configuration:
`coala --bears=ClangCloneDetectionBear,ClangSimilarityBear --show-bears`

***

# Sources

[1] Roy, Chanchal K., James R. Cordy, and Rainer Koschke.
"Comparison and evaluation of code clone detection techniques and tools:
A qualitative approach." *Science of Computer Programming* 74.7 (2009): 470-495.

[2] Yuan, Yang, and Yao Guo. "CMCD: Count matrix based code clone detection."
*Software Engineering Conference (APSEC), 2011 18th Asia Pacific.*
IEEE, 2011.

[3] Yuan, Yang, and Yao Guo.
"Boreas: an accurate and scalable token-based approach to code clone detection."
*Proceedings of the 27th IEEE/ACM International Conference on Automated Software
Engineering.* ACM, 2012.

[4] Chen, Xiliang, Alice Yuchen Wang, and Ewan Tempero.
"A replication and reproduction of code clone detection studies."
*Proceedings of the Thirty-Seventh Australasian Computer Science
Conference-Volume 147.* Australian Computer Society, Inc., 2014.

***

# Questions?
