# Investigating the Influence of Different Counting Conditions in Count Matrix based Code Clone Detection
#### by Lasse Schuirmann
##### Institute for Software Systems
### June 4, 2015

***

# Contents

- The Problem
  - Why do we Care About Code Clones?
  - What is a Code Clone?
  - Why is it Difficult to Detect Them?
- The Solution
  - A Similarity Definition
  - Grouping Variables
  - Comparing Functions
  - False Positive Avoidance
- Summary
- Investigation and Evaluation
  - Creating a Test Dataset
  - Visualize Results
  - Improvement Possibilities
  - Reproducability

***

# The Problem

***

## Why do we Care About Code Clones?

- **Duplicate code smells**!
- **Duplicate code** means **duplicate bugs**.
- **Less code** is **easier to maintain**.

***

## Why do we Care About Code Clones?

#### What do we Want?

- We want to detect **opportunities** to **improve our software**.
- We want to find several functions that we can **join reasonably** into **one
  better** version.
- We want to detect the **most useful** results.

***

## What is a Code Clone?

> A **code fragment** (...) is a **clone** of **another code fragment** if they
> are **similar by some given definition of similarity** (...).

*- Roy et al., Comparison and evaluation of code clone detection techniques and
tools: A qualitative approach*

***

## What is a Code Clone?

#### An Example

```C
bool test_search_for_empty() {
    int result;
    // Test without regexes active
    result = search_for("", "not_in_there", false)
    return result == -1;
}
```

```C
bool test_search_for_regex_empty() {
    int result;
    // Test with regexes active
    result = search_for("", "not_in_there", true)
    return result == -1;
}
```

***

## Why is it Difficult to Detect Them?

- **Similarity** is **hard to define** in a **useful** way.
- There are **a lot possible clone combinations** that need to be evaluated.
- Code needs to be **understood** to a **certain degree** to find **similar
  functionality** instead of **just similar text**.

***

# The Solution

#### For C

***

# The Solution

#### Disclaimer

Who did do this?

***

## A Similarity Definition

- Calculate **vectors** that **represent** the **identity** of each
  **variable**.
- **Count different kinds** of **usages** for each **variable**.
- Essentially **hashing** the variable. (**Order** of **usages** and **name** is
  **unpreserved**.)

***

## A Similarity Definition

#### Counting Conditions

A **counting condition** is used to **count variables under certain specified
circumstances**.

Example:

`(DECLARED, ASSIGNED, RETURNED)`

***

## A Similarity Definition

#### Example of a Count Vector

```C
int result; // DECLARED
// Test with regexes active
result = search_for("", "not_in_there", true) // ASSIGNED
return result == -1; // RETURNED
```

`(1, 1, 1)`

***

## A Similarity Definition

#### Comparing Count Vectors

```python
class CountVector:
    def difference(self, other):
        maxabs = sum(max(x, y)**2 for x, y in zip(self, other))
        if maxabs == 0: return 0
        return sum((x-y)**2 for x, y in zip(self, other))/maxabs
```

- **Zeros in both vectors don't influence** the result.
- The difference value is **between 0 and 1**, i.e. **normalized**.
- This difference value is **symmetric**.

***

## Grouping Variables

- **Comparing all elements** of a set **with all others** has a **complexity**
  of `O(n*(n-1))`.
- We only have **finite time** available.
- Let's **only** search for **duplicate functions** and exclude all other
  possible combinations. (Maybe another thesis.)
- A **function** is **represented** by a **matrix of count vectors**, the
  **count matrix**.
- **Global variables** are ignored. **Member variables** do not exist in
  **procedural languages**.
  - This is a **limitation** of this **algorithm implementation**!

***

## Comparing Functions

- Find the **best match** for all **count vectors** in the functions.
  - We **compare all count vectors** individually with each other and put them
    into a **cost matrix** to do the matching.
- This matching algorithm has currently a **complexity** `O(n^3)` while `n` is
  the maximum of the number of count vectors over both functions.

***

## Comparing Functions

### Creating the Cost Matrix

```python
cost_matrix = [[cv1.difference(cv2)
                for cv2 in cm2.values()]
               for cv1 in cm1.values()]
```

**Difference of 1** for variables that only exist in one function i.e. are
**unmatched**.

The cost matrix represents a **bipartite graph** holding the **difference** for
all **possible variable pairs** as **weightings**.

***

# False Positive Avoidance

- Need to **avoid** a few **common code clones** like **getters** and
  **setters** which are **no helpful results**.
- **Heuristics**:
  - **Exclude small** functions. (Getters, setters are all clones but not
    helpful.)

***

# Summary

- Create a **count vector** for each **variable**.
- **Compare functions** with **graph matching**.
- **Compare the result** with a **threshold**.

***

# Investigation and Evaluation

***

## Creating a Test Dataset

- Group **samples** in **clones** and **non-clones**.
- Write a **unittest**.

***

## Creating a Test Dataset

#### How to Obtain the Test Dataset

- **Clone** samples:
  - **Roy et al.** created a **test clone dataset** and **published** it.
- **Non-clone** samples:
  - Taking **arbitrary functions** out of my **kernel**.

**Running** on **real code**.

***

## Creating a Test Dataset - Non-Clone Example

```C
void *memcpy(void *restrict dst, const void *restrict src, int len) {
    char *dt = (char*)dst;
    char *sc = (char*)src;
    while(len--) *dt++ = *sc++;
    return dst;
}
```

```C
void *memset(void * dst, int val, int len) {
    char *dt = (char*)dst;
    while(len--) *dt++ = (char)val;
    return dst;
}
```

***

## Creating a Test Dataset - Clone Example

```C
static void scrollUp() {
    uint16_t i;
    volatile unsigned char *videoram = (unsigned char *)FB_MEM_LOCATION;
    for(i=0; i<(FB_LINES-1)*FB_COLUMNS*2; i++)
        videoram[i]=videoram[i+(FB_COLUMNS * 2)];
    for(; i<FB_LINES*FB_COLUMNS*2; i++)
        if(i % 2) videoram[i]=COLCODE(STDFG, STDBG);
        else videoram[i]=BLANK; }
```

```C
void clearScreen() {
    uint16_t i;
    volatile unsigned char *videoram = (unsigned char *)FB_MEM_LOCATION;
    for(i=0; i<FB_LINES*FB_COLUMNS*2; i++)
        if(i % 2) videoram[i]=COLCODE(STDFG, STDBG);
        else videoram[i]=BLANK;
    setCursor(GET_POS(0,0)); }
```

***

## Creating a Test Dataset

**All tests pass.**

***

## Visualize Results

![Result Visualization](https://storage.googleapis.com/b.swipeusercontent.com/rSefw9nz_hN_F67_ew1kIPNDwQCgsw-672.png)

- **Red**: *Clones*
- **Green**: *Non-clones*
- **x-Axis**: *Difference value*

**Goal**: **Maximize area between red and green**.

***

## Improvement Possibilities

- There are still **false positives** when running it on the **linux kernel**.
  - **Count vector comparision** is **normalized**. Should **big count vectors
    matter more**?
  - **Function calls** could be **counted** too!
- **Global** and **member** variables are **ignored** in this implementation.
  This problem will probably be adressed after the thesis.
- **Performance**.
  - **Fast bipartite graph matching algorithms** with **error estimation** can
    be used to **fastly sort out** "easy" **non-clones**.
- **Evaluation**:
  - **Comparision** with an **alternative tool** is **due**.

***

## Improvement Possibilities

#### Other Languages

- The **algorithm** can be used for **other languages without modification**.
- **Creation of count vectors** is **language specific**.
- A **new language** will need **new tweaking** of **counting conditions**.

***

## Improvement Possibilities

#### Reusing Count Vectors

- The **count vectors** can be used to **identify other problems** too.
- **Big count vectors** indicate a **complexly used variable**.
- **Count vectors** with values in **higher level loop counts** indicate
  **complex functions**.

***

## Reproducability

```
git clone https://github.com/coala-analyzer/coala.git coala
cd coala
./run_tests.py -m
```

```
cd coala
sudo ./setup.py install  # Install coala
cd ../your_c_project
coala -s --bears=ClangCloneDetectionBear --files=**/*.c  # -s saves config
```

Configuration:
`coala --bears=ClangCloneDetectionBear,ClangSimilarityBear --show-bears`

***

# Sources

[1] Roy, Chanchal K., James R. Cordy, and Rainer Koschke.
"Comparison and evaluation of code clone detection techniques and tools:
A qualitative approach." *Science of Computer Programming* 74.7 (2009): 470-495.

[2] Yuan, Yang, and Yao Guo. "CMCD: Count matrix based code clone detection."
*Software Engineering Conference (APSEC), 2011 18th Asia Pacific.*
IEEE, 2011.

[3] Yuan, Yang, and Yao Guo.
"Boreas: an accurate and scalable token-based approach to code clone detection."
*Proceedings of the 27th IEEE/ACM International Conference on Automated Software
Engineering.* ACM, 2012.

[4] Chen, Xiliang, Alice Yuchen Wang, and Ewan Tempero.
"A replication and reproduction of code clone detection studies."
*Proceedings of the Thirty-Seventh Australasian Computer Science
Conference-Volume 147.* Australian Computer Society, Inc., 2014.

***

# Questions?
