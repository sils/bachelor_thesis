% Investigating the Influence of Different Counting Conditions in Count Matrix
  based Code Clone Detection
% Lasse Schuirmann
% Institute for Software Systems, September 24, 2015

## Contents

- Introduction
- Count Matrix based Code Clone Detection
- Algorithm Modifications
- Optimization Algorithms
- Evaluation
- Recommendations

# Introduction

## Example of a Code Clone

```C
int factorial(int x) {
    int result = x;
    while(x > 2) result *= --x;
    return result;
}
```

```C
int factorial(int x) {
    int result = x;
    for(x--; x > 1; --x) result *= x;
    return result;
}
```

# Count Matrix Based Code Clone Detection Algorithm

## Variable Representation (Example)

```C
int factorial(int x) {
    int result = x;
    for(x--; x > 1; --x)
        result *= x;
    return result;
}
```

Counting Conditions `(RETURNED, USED)` for `result`

- Naive count vector: $(1,3)$

## Grouping Variables

- Search for **duplicate functions**
- **Represent function** by its **variables**
- **Best match** of count vectors

## Summary

~~~~ {.dot .Grankdir:LR}
digraph {
  "Fragment a" -> "Count Vector a";
  "Fragment a" -> "Count Vector b";
  "Fragment b" -> "Count Vector c";
  "Fragment b" -> "Count Vector d";
  "Count Vector a" -> "Count Matrix a"
  "Count Vector b" -> "Count Matrix a"
  "Count Vector c" -> "Count Matrix b"
  "Count Vector d" -> "Count Matrix b"
  "Count Matrix a" -> Difference
  "Count Matrix b" -> Difference
}
~~~~

# Algorithm Modifications

## Weightings

- Naive count vector: $(1,3)$
- Weighting: $(1,0.5)$
- Result: $(1,1.5)$

## Comparing Count Matrices (Average)

$$
CM\_diff_1(\mathbf{matches}) =
\sum\limits_{\mathbf{a},\mathbf{b}=\mathbf{matches}}
\frac
   {||\mathbf{a}-\mathbf{b}||}
   {\sqrt{\sum\limits_{i=1}^{dim(\mathbf{a})} max(\mathbf{a}_i, \mathbf{b}_i)^2}}
\cdot \frac{1}{dim(matches)}
$$

> - Normalize **Variables**
> - All variables are equally important!

## Comparing Count Matrices (Function Wise)

$$
CM\_diff_2(\mathbf{matches}) =
\frac {
  \sum\limits_{\mathbf{a},\mathbf{b}=\mathbf{matches}} ||\mathbf{a}-\mathbf{b}||
}{
  \sum\limits_{\mathbf{a},\mathbf{b}=\mathbf{matches}}
  \sqrt{\sum\limits_{i=1}^{dim(\mathbf{a})} max(\mathbf{a}_i, \mathbf{b}_i)^2}
}
$$

- Normalize **Functions**

## Postprocessing

Prefer big functions!

- Exponential
- Polynomial

## Counting "Things"

We can count more than just variables!

- Constants!
- Function invocations!
- Variables (of course).

## Syntactic Equivalents

- `if` versus `switch-case`
- `for` versus `while`

# Optimization Algorithms

## Threshold Determination

![Difference Values of Clones (Red) and Non-Clones (Green)](visualize.png)

## Weighting Determination

![Weighting Optimization](weightings.png)

# Evaluation

## Test Dataset

- **Clone** samples (22 clone pairs)
- **Non-clone** samples (27 unique functions)

## Modifications

Normalization Type    Postprocessing             Unoptimized         Optimized
--------------------- ------------------- ------------------ -----------------
Average               Off                 $\mathbf{-0.3628}$ $-0.198$
Average               Polynomial          $-0.2712$          $-0.173$
Average               Exponential         $-0.2721$          $-0.149$
Average               Both                $-0.2034$          $-0.130$
Function Wise         Off                 $-0.1512$          $0.069$
Function Wise         Polynomial          $-0.1127$          $\mathbf{0.087}$
Function Wise         Exponential         $-0.1134$          $0.052$
Function Wise         Both                $-0.0845$          $0.065$

Table: Fitness of Different Modifications

## Testing on Real Code (Generated Config)

Project   Directory  False  True   F. P. Rate     LOC
--------- ---------- ------ ------ -------------- ------
Yafos     `.`        0      3      0%             1649
TE3D      `.`        1      95     1%             3114
CPython   `Parser`   8      11     42%            4169
Linux     `fs/ext2`  4      3      57%            6546
Builder   `.`        35     6      85%            91848

Table: Threshold 0.308

## Relation: Threshold to False Positive Rate

![](graph.png)\

<!-- Additional newline needed because of escape. -->

## Testing on Real Code (Lowered Threshold)

Project   Directory  False  True   F. P. Rate     Missed LOC
--------- ---------- ------ ------ -------------- ------ ------
Yafos     `.`        0      1      0%             2      1649
TE3D      `.`        0      34     0%             60     3114
CPython   `Parser`   0      4      0%             7      4169
Linux     `fs/ext2`  0      3      0%             0      6546
Builder   `.`        3      5      38%            1      91848

Table: Threshold 0.185

# Recommendations

## Open Questions

- Is counting constants and function invocations useful?
- Are weightings useful?

## Recommendations

- Use lexical approach
- Detect sub-function clones
- Improve performance
- Improve language independence

## Sources

Roy, Chanchal K., James R. Cordy, and Rainer Koschke.
"Comparison and evaluation of code clone detection techniques and tools:
A qualitative approach."

Yuan, Yang, and Yao Guo. "CMCD: Count matrix based code clone detection."

Yuan, Yang, and Yao Guo.
"Boreas: an accurate and scalable token-based approach to code clone detection."

Chen, Xiliang, Alice Yuchen Wang, and Ewan Tempero.
"A replication and reproduction of code clone detection studies."

# Questions?

## Testing on Real Code (Generated Config)

Project   Directory  False  True   F. P. Rate     LOC
--------- ---------- ------ ------ -------------- ------
Yafos     `.`        0/0    3/6    0%/0%          1649
TE3D      `.`        1/2    95/57  1%/3%          3114
CPython   `Parser`   8/13   11/16  42%/45%        4169
Linux     `fs/ext2`  4/8    3/6    57%/57%        6546
Builder   `.`        35/28  6/12   85%/70%        91848

Table: Threshold 0.308

## Testing on Real Code (Lowered Threshold)

Project   Directory  False  True   F. P. Rate     Missed LOC
--------- ---------- ------ ------ -------------- ------ ------
Yafos     `.`        0/0    1/2    0%/0%          2/4    1649
TE3D      `.`        0/0    34/47  0%/0%          60/10  3114
CPython   `Parser`   0/0    4/8    0%/0%          7/8    4169
Linux     `fs/ext2`  0/0    3/6    0%/0%          0/0    6546
Builder   `.`        3/6    5/10   38%/38%        1/2    91848

Table: Threshold 0.185

## Normalization Exploiting Test Excluded

Normalization Type    Fitness
------------------- ---------
Average             $-0.2541$
Function Wise       $-0.1512$

Table: Fitness of Different Normalization Types (Normalization Exploiting Test Excluded)

## Postprocessing Functions

$$f(s) = \frac{e^{-(s-1)}}{4}+0.75 \,\,\,\,\,\,\,\,\,\,\,\,\,\,\, g(s) = \frac{3s+1}{4s}$$

![Postprocessing Function Visualization](postproc.png)

## Implementation Structure

~~~~ {.dot .Grankdir:TB}[Top-Level Implementation Structure]
digraph {
  CloneDetectionRoutines -> ClangFunctionDifferenceBear
  ClangCountVectorCreator -> ClangFunctionDifferenceBear
  ClangFunctionDifferenceBear -> ClangCloneDetectionBear
  ClangFunctionDifferenceBear -> ClangCloneDetectionBenchmarkBear
  ClangCountVectorCreator -> ClangCCOptimizeBear
  CloneDetectionRoutines -> ClangCCOptimizeBear
}
~~~~

## Sub-Function Clones

```C
static void scrollUp() {
    uint16_t i;
    volatile unsigned char *videoram =
        (unsigned char *)FB_MEM_LOCATION;
    for(i=0; i<(FB_LINES-1)*FB_COLUMNS*2; i++)
        videoram[i]=videoram[i+(FB_COLUMNS * 2)];
    for(; i<FB_LINES*FB_COLUMNS*2; i++)
        if(i % 2) videoram[i]=COLCODE(STDFG, STDBG);
        else videoram[i]=BLANK;
    }

static void clearScreen();
```
